# Login

This is an example of a login in PHP and MySQL using sessions and SHA2 encryption

## How to use
  * clone this repo to the apache directory
  * run the `db.sql` file to create the example database
  * go to `localhost/login_tryout/` and enjoy

## Author
  * Miguel Asencio
