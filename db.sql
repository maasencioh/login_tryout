CREATE DATABASE IF NOT EXISTS login_example;
USE login_example;
CREATE TABLE IF NOT EXISTS users (
    usr_fname VARCHAR(30) NOT NULL,
    usr_lname VARCHAR(30) NOT NULL,
    usr_email VARCHAR(30) NOT NULL,
    usr_pass VARCHAR(56) NOT NULL,
    PRIMARY KEY (usr_email)
);
INSERT INTO users (usr_fname, usr_lname, usr_email, usr_pass) VALUES ('James', 'Bond', 'bond@james.bond', SHA2('123', 224));
